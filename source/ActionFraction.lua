--	Title: ActionFraction v1.7
--
--	Author: Alpha_Male (alpha_male@speakeasy.net)
--
--	Description: Customizable window that displays the total and current action points values for the player.
--
--	Features: * Displays Total and Current Action Points
--						* Customizable window scale and position
--						* Right click options window which includes:
--							* Lock/Unlock window location
--							* Ability to quickly set display window to preset screen locations
--							* Customizable font size
--							* Color Code Current Action Point levels
--							* Customizable auto hide or fade options (can match Player Window)
--							* Reset to defaults
--						* All options are saved per character
--						* Optional LibSlash support for expanded menu options
--						* Multiple language support
--
--	Files: \language\ActionFractionLocalization.lua
--	 			 \source\ActionFraction.lua
--				 \source\ActionFraction.xml
--	 			 \ActionFraction.mod
--	 			 \ActionFraction_Install.txt
--	 			 \ActionFraction_Readme.txt
--			
--	Version History: 1.0 Initial Release
--									 1.1 Maintenance Update and Fixes
--											 - Version update for latest patch release
--											 - Window Alpha and Fade Fixes (entire system rework due to PlayerWindow changes)
--								 	 1.2 Maintenance Update
--											 - Version update for latest 1.4 patch release
--									 1.3 Maintenance Update and New Features
--											 - Updated mod version information for 1.4.1
--											 - Added color coded current AP functionality and associated right click menu option
--									 1.4 Bug Fixes and New Features
--											 - Fixed offsets for Preset Location -> Action Bar, should be centered better for all font sizes
--											 - Color coded current AP is on by default
--											 - Fixed saved setting issue that was causing mod to not load for some people
--											 - Added separators for the Right-Click options context menu
--									 1.5 Maintenance Update and New Features
--											 - Updated mod version information for 1.4.3
--											 - Added optional LibSlash support for disabling right-click options menu (makes entire mod window click thru)
--											 - Fixes for "Reset to Defaults" saved variables and right-click menu checkbox display
--											 - Added additional check for AP changed game events (maximum AP)
--									 1.6 Minor Bug Fixes
--											 - Hitpoint eventhandler unregister minor fix
--									 1.7 Maintenance Update and Minor Bug Fixes
--											 - Updated mod version information for 1.4.5
--											 - Initialization display string format change
--
--  Supported Versions: Warhammer Online v1.4.5
--
--	Dependencies: None
--
--	Future Features: Continued updates for multiple language support.
--
--	Known Issues:  None
--
--	Additional Credits: None
--
--	Special Thanks:	EA/Mythic for a great game and for releasing the API specs
--								  The War Wiki (www.thewarwiki.com) for being a great source of knowledge for WAR mod development
--									www.curse.com and www.curseforge.com for hosting WAR mod files and projects
--								  Trouble INC guild of Badlands for their support
--
--	License/Disclaimers: This addon and it's relevant parts and files are released under the 
--											 GNU General Public License version 3 (GPLv3).
--

------------------------------------------------------------------
----  Global Variables
------------------------------------------------------------------
if not ActionFraction then
	ActionFraction = {}
end

if not ActionFractionWindow then
	ActionFractionWindow = {}
end

-- Addon Info
ActionFraction.Title = L"ActionFraction"
ActionFraction.Version = 1.7

-- Font Definitions
ActionFractionWindow.FontSizeSmall 	= "font_clear_small_bold"
ActionFractionWindow.FontSizeMedium = "font_clear_medium_bold"
ActionFractionWindow.FontSizeLarge 	= "font_clear_large_bold"

------------------------------------------------------------------
----  Local Variables
------------------------------------------------------------------

-- Name shortcut variable
local windowName	= "ActionFractionWindow"

-- Hook variables used for hooking into player window fade in and out and
-- for handling interface shutdown.
local hookMouseOverPortrait
local hookMouseOverPortraitEnd
local hookShutdownPlayInterface

-- Initialize variable that tracks whether to autohide ActionFraction with the player window
local bAutoHide = false

-- Initialize variables used to track various conditions of the player portrait (styled after EA/Mythic playerwindow.lua)
local isMouseOverPortrait   = false
local isFadeIn              = false -- Was the last fade a fade in (true) or a fade out (false)
local fadeOutAnimationDelay = 0

-- Default mod window dimensions
local winWidth 		= 100
local winHeight 	= 28

-- Localization Text
local LOC_TEXT
if not LOC_TEXT then
	LOC_TEXT = ActionFraction.wstrings[SystemData.Settings.Language.active] 
end

-- LibSlash string used for message output
local strLibSlashCmds = nil

----------------------------------------------------------------
-- Local/Utility Functions
----------------------------------------------------------------

-- NOTE: This function is based on UpdateStatusContainerVisibility() from playerwindow.lua because
-- it was made local in that script and was not able to be accessed by ActionFraction.
local function UpdateActionFractionVisibility()
    local show = ( SystemData.Settings.GamePlay.preventHealthBarFade
                or GameData.Player.inAgro
                or isMouseOverPortrait
                or ( GameData.Player.hitPoints.current < GameData.Player.hitPoints.maximum )
                or ( GameData.Player.actionPoints.current < GameData.Player.actionPoints.maximum ) )
                or (not ActionFraction.Settings.bAutoHide)
    
    -- note that the label alpha and associated text is what is being controlled, this allows
    -- the main window to remain and right click options to work on it
    local currentAlpha = WindowGetAlpha( windowName.."LabelCurrentAP" )
    
    if ( show )
    then
        fadeOutAnimationDelay = 0
        -- Action Fraction Window Label should be shown. Fade it in (unless we're already in the process of fading it in)
        if ( ( currentAlpha == 0.0 ) or ( ( currentAlpha < 1.0 ) and not isFadeIn ) )
        then
            isFadeIn = true
            WindowSetShowing( windowName.."LabelCurrentAP", true )
            WindowSetShowing( windowName.."LabelMaximumAP", true )
            WindowStartAlphaAnimation( windowName.."LabelCurrentAP", Window.AnimationType.SINGLE_NO_RESET, 
            													 currentAlpha, 1.0, 0.5, 
            													 false, 0, 0 )
						WindowStartAlphaAnimation( windowName.."LabelMaximumAP", Window.AnimationType.SINGLE_NO_RESET, 
            													 currentAlpha, 1.0, 0.5, 
            													 false, 0, 0 )
        end
    else
        -- Action Fraction Window Label should be hidden. Fade it out (unless already in the process of
        -- fading it out, or already in the "delay" phase)
        if ( ( fadeOutAnimationDelay == 0 ) and ( ( currentAlpha == 1 ) or ( ( currentAlpha > 0.0 ) and isFadeIn ) ) )
        then
            fadeOutAnimationDelay = PlayerWindow.FADE_OUT_ANIM_DELAY
        end
    end
end

------------------------------------------------------------------
----  Core Functions
------------------------------------------------------------------

function ActionFraction.Start()
		-- Wait for the player to enter the world or for the interface to be loaded/reloaded.
		RegisterEventHandler( SystemData.Events.ENTER_WORLD, "ActionFraction.Initialize" )
		RegisterEventHandler( SystemData.Events.INTERFACE_RELOADED, "ActionFraction.Initialize" )
end

function ActionFraction.Initialize()
		TextLogAddEntry( "Chat", SystemData.ChatLogFilters.SHOUT, L"["..ActionFraction.Title..L"]: "..L" Version "..(wstring.format( L"%.01f",ActionFraction.Version))..L" Initialized" )
		DEBUG ( towstring( "ActionFraction loaded" ))

		-- Optional LibSlash support
		if (LibSlash) then
				-- Define LibSlash handler function
				local slashHandler = function(arg) ActionFraction.SlashHandler(arg) end
				
				-- Check if commands to register are already taken by another mod, if so, output an error message,
				-- if not then register them
				if not LibSlash.IsSlashCmdRegistered("af") then
						LibSlash.RegisterWSlashCmd("af", slashHandler)
						strLibSlashCmds = "/af"
				else
						TextLogAddEntry( "Chat", 0, LOC_TEXT.ERROR_LIBSLASH_MESSAGE01 )
						DEBUG ( towstring( "LibSlash Command \"af\" already registered." ))
				end
				
				if not LibSlash.IsSlashCmdRegistered("ActionFraction") then
						LibSlash.RegisterWSlashCmd("ActionFraction", slashHandler)
						if strLibSlashCmds ~= nil then
							strLibSlashCmds = "/af or /ActionFraction"
						else
							strLibSlashCmds = "/ActionFraction"
						end
				else
						TextLogAddEntry( "Chat", 0, LOC_TEXT.ERROR_LIBSLASH_MESSAGE02 )
						DEBUG ( towstring( "LibSlash Command \"ActionFraction\" already registered." ))					
				end
				
				if strLibSlashCmds ~= nil then
						TextLogAddEntry( "Chat", 0, towstring(LOC_TEXT.MENU_LIBSLASH_MESSAGE01A) .. towstring(strLibSlashCmds) .. towstring(LOC_TEXT.MENU_LIBSLASH_MESSAGE01B) )
				end
		end

		-- Initialize saved data (SavedVariables.lua)
		
		-- Options settings
		if not ActionFraction.Settings then
				ActionFraction.Settings = {
							FontSize = ActionFractionWindow.FontSizeSmall,
							bColorCodeCurrentAP = true,
							bAutoHide = false,
							Version = ActionFraction.Version,
							bDisableContextMenu = false,
		  		}
		end

		-- Handle previous versions of ActionFraction
		if (ActionFraction.Settings.Version < 1.4) then

				-- Wipe out previous save data
				ActionFraction.Settings = {}

				-- Set default save data
				ActionFraction.Settings = {
						FontSize = ActionFractionWindow.FontSizeSmall,
						bColorCodeCurrentAP = true,
						bAutoHide = false,
						Version = ActionFraction.Version,
						bDisableContextMenu = false,
		  	}
		end

		-- Add new saved settings for older versions
		if (ActionFraction.Settings.bDisableContextMenu == nil) or (ActionFraction.Settings.Version < 1.5) then
				-- Add SaveVariables setting for whether right click context menu option is disabled via LibSlash
				ActionFraction.Settings.bDisableContextMenu = false
		end

		-- Update the saved data version
		if ActionFraction.Settings.Version < ActionFraction.Version then
				ActionFraction.Settings.Version = ActionFraction.Version
		end
		
		-- Prevent player from not having right-click menu access to ActionFraction if they had LibSlash, disabled the menu and 
		-- then for some reason LibSlash is gone or disabled
		if (LibSlash == false) or (LibSlash == nil) then
		DEBUG (towstring("Libslash nil or false"))
				if (ActionFraction.Settings.bDisableContextMenu == true) then
					DEBUG (towstring("Enabling Right-Click Context Menu"))
					TextLogAddEntry( "Chat", 0, LOC_TEXT.MENU_LIBSLASH_MESSAGE_ENABLED )
					ActionFraction.Settings.bDisableContextMenu = false
				end
		end

		-- Unregister the initial start up event handlers.
		UnregisterEventHandler( SystemData.Events.ENTER_WORLD, "ActionFraction.Initialize" )
		UnregisterEventHandler( SystemData.Events.INTERFACE_RELOADED, "ActionFraction.Initialize" )
			
		-- ActionFraction has now initialized so initialize the ActionFraction Window and as a result 
		-- the associated Action Point values.
		ActionFractionWindow.Initialize()
end

function ActionFractionWindow.Initialize()
		-- Create the display window
		CreateWindow(windowName, true)

		-- Set whether window is able to be clicked (right click for context menu)
		WindowSetHandleInput(windowName, not ActionFraction.Settings.bDisableContextMenu)

    -- Initialize the ContextMenu Color Coded AP Option
    CreateWindowFromTemplate ( "ActionFractionWindowContextColorCodeCurrentAP", "ActionFractionWindowContextCheckBox", "Root" )
		LabelSetText( "ActionFractionWindowContextColorCodeCurrentAPCheckBoxLabel", LOC_TEXT.CURRENTAP_COLOR_CODING )
		WindowRegisterCoreEventHandler("ActionFractionWindowContextColorCodeCurrentAP", "OnLButtonUp", "ActionFractionWindow.ToggleColorCodeCurrentAP")
		WindowSetShowing("ActionFractionWindowContextColorCodeCurrentAP", false)
		
		ButtonSetPressedFlag("ActionFractionWindowContextColorCodeCurrentAPCheckBox", ActionFraction.Settings.bColorCodeCurrentAP)

    -- Initialize the ContextMenu AutoHide Option
    CreateWindowFromTemplate ( "ActionFractionWindowContextAutoHide", "ActionFractionWindowContextCheckBox", "Root" )
		LabelSetText( "ActionFractionWindowContextAutoHideCheckBoxLabel", GetChatString( StringTables.Chat.LABEL_CHAT_TAB_AUTOHIDE ) )
		WindowRegisterCoreEventHandler("ActionFractionWindowContextAutoHide", "OnLButtonUp", "ActionFractionWindow.ToggleAutoHide")
		WindowSetShowing("ActionFractionWindowContextAutoHide", false)
		
		ButtonSetPressedFlag("ActionFractionWindowContextAutoHideCheckBox", ActionFraction.Settings.bAutoHide)

		-- Register the window with the layout editor to allow it to be customized
		LayoutEditor.RegisterWindow( windowName,
	   														L"Action Fraction",
	   														L"Displays character's current Action Points",
	   														true, 
	   														true,
	   														true, 
	   														nil )

		-- Set up the hook functions that tie into the player window
		hookMouseOverPortrait = PlayerWindow.MouseOverPortrait
		hookMouseOverPortraitEnd = PlayerWindow.MouseOverPortraitEnd

		PlayerWindow.MouseOverPortrait = ActionFractionWindow.MouseOverPortrait
		PlayerWindow.MouseOverPortraitEnd = ActionFractionWindow.MouseOverPortraitEnd

		-- Hooks into Play Interface Shutdown
		hookShutdownPlayInterface = InterfaceCore.ShutdownPlayInterface
		InterfaceCore.ShutdownPlayInterface = ActionFractionWindow.Shutdown	

		-- Register the event that causes the Action Point value updates.
		WindowRegisterEventHandler( windowName, SystemData.Events.PLAYER_CUR_ACTION_POINTS_UPDATED, "ActionFractionWindow.UpdateActionPoints" )
		WindowRegisterEventHandler( windowName, SystemData.Events.PLAYER_MAX_ACTION_POINTS_UPDATED, "ActionFractionWindow.UpdateActionPoints" )

		-- Register events that will cause PlayerStatuContainer to change visibility and thus, the ActionFractionWindow.
		WindowRegisterEventHandler( windowName, SystemData.Events.PLAYER_CUR_HIT_POINTS_UPDATED, "ActionFractionWindow.UpdateCurrentHitPoints")
    WindowRegisterEventHandler( windowName, SystemData.Events.PLAYER_AGRO_MODE_UPDATED, "ActionFractionWindow.OnAgroModeUpdated")

		-- Register event for if the player changes their fade option for the player window
		WindowRegisterEventHandler( windowName, SystemData.Events.PLAYER_HEALTH_FADE_UPDATED, "ActionFractionWindow.UpdateBasedOnUserSettings")

		-- Register event for if the player changes zones (end of world reloading)
		RegisterEventHandler( SystemData.Events.LOADING_END, "ActionFractionWindow.UpdateVisibility")
	
		-- Set the font size and type based on saved settings
		LabelSetFont( windowName.."LabelCurrentAP", ActionFraction.Settings.FontSize, WindowUtils.FONT_DEFAULT_TEXT_LINESPACING )
		LabelSetFont( windowName.."LabelMaximumAP", ActionFraction.Settings.FontSize, WindowUtils.FONT_DEFAULT_TEXT_LINESPACING )
		
		-- Update the Action Point values
		ActionFractionWindow.UpdateActionPoints()
end

function ActionFractionWindow.UpdateActionPoints()
		local currentAP = GameData.Player.actionPoints.current
		local currentAPPercent = (currentAP/GameData.Player.actionPoints.maximum)*100

		-- Update the label text with the current Action Point values
		LabelSetText(windowName.."LabelCurrentAP", L""..currentAP)
		
		-- Update the label text color coding if this feature is enabled
		if (ActionFraction.Settings.bColorCodeCurrentAP == true) then 
			ActionFractionWindow.SetPercentageBasedColors (windowName.."LabelCurrentAP", currentAPPercent)
		else
			LabelSetTextColor(windowName.."LabelCurrentAP", DefaultColor.WHITE.r, DefaultColor.WHITE.r, DefaultColor.WHITE.r)
		end
		
		-- Display the maximum AP available or the player AP pool
		LabelSetText(windowName.."LabelMaximumAP", L"/"..GameData.Player.actionPoints.maximum)		
		
		UpdateActionFractionVisibility()
end

function ActionFractionWindow.SetPercentageBasedColors(label, value)		
		if(value >= 75) then
			LabelSetTextColor(label, 150, 255, 65) 	-- Green
		elseif(value >= 50) then
			LabelSetTextColor(label, 255, 255, 70) 	-- Yellow
		elseif(value >= 25) then
			LabelSetTextColor(label, 255, 170, 70) 	-- Orange
		elseif(value >= 0) then
			LabelSetTextColor(label, 255, 70, 70)		-- Red
		end
end

function ActionFractionWindow.UpdateVisibility()
		UpdateActionFractionVisibility()
end

function ActionFractionWindow.ToggleColorCodeCurrentAP()
		-- Toggle the boolean state of the color code current ap variable
		ActionFraction.Settings.bColorCodeCurrentAP = not ActionFraction.Settings.bColorCodeCurrentAP
		ButtonSetPressedFlag("ActionFractionWindowContextColorCodeCurrentAPCheckBox", ActionFraction.Settings.bColorCodeCurrentAP)
		
		-- Update action points to reset color changes
		ActionFractionWindow.UpdateActionPoints()
end

function ActionFractionWindow.ToggleAutoHide()
		-- Toggle the boolean state of the autohide variable
		ActionFraction.Settings.bAutoHide = not ActionFraction.Settings.bAutoHide
		ButtonSetPressedFlag("ActionFractionWindowContextAutoHideCheckBox", ActionFraction.Settings.bAutoHide)

		UpdateActionFractionVisibility()
end

function ActionFractionWindow.OnAgroModeUpdated()
    UpdateActionFractionVisibility()
end

function ActionFractionWindow.UpdateCurrentHitPoints()
    if( GameData.Player.hitPoints.current ~= 0 ) then
        UpdateActionFractionVisibility()
    end
end

function ActionFractionWindow.MouseOverPortrait( ... )
		hookMouseOverPortrait( ... )

    isMouseOverPortrait = true
    UpdateActionFractionVisibility()
end

function ActionFractionWindow.MouseOverPortraitEnd( ... )
		hookMouseOverPortraitEnd( ... )

    isMouseOverPortrait = false
    UpdateActionFractionVisibility()
end

function ActionFractionWindow.UpdateBasedOnUserSettings()
		fadeOutAnimationDelay = 0
		UpdateActionFractionVisibility()
end

function ActionFractionWindow.OnUpdate( timePassed )
    -- note that the label alpha and associated text is what is being controlled, this allows
    -- the main window to remain and right click options to work on it
    if (fadeOutAnimationDelay > 0) then
        if ( (WindowGetAlpha( windowName.."LabelCurrentAP" ) == 1.0) or (WindowGetAlpha( windowName.."LabelMaximumAP" ) == 1.0) ) -- Don't begin fade out delay until Action Fraction window is fully shown
        then
            fadeOutAnimationDelay = fadeOutAnimationDelay - timePassed
            if ( fadeOutAnimationDelay <= 0 ) then
                fadeOutAnimationDelay = 0
                isFadeIn = false
                WindowStartAlphaAnimation ( windowName.."LabelCurrentAP", 
                														Window.AnimationType.SINGLE_NO_RESET_HIDE, 
                														1.0, 0.0, 2.0,
                														false, 0, 0 )
                WindowStartAlphaAnimation ( windowName.."LabelMaximumAP", 
                														Window.AnimationType.SINGLE_NO_RESET_HIDE, 
                														1.0, 0.0, 2.0,
                														false, 0, 0 )
            end
        end
    end
		WindowSetShowing(windowName.."LabelCurrentAP", true)
		WindowSetShowing(windowName.."LabelMaximumAP", true)
end

function ActionFractionWindow.Shutdown( ... )
		-- Unregister events on shutdown
		WindowUnregisterEventHandler( windowName, SystemData.Events.PLAYER_CUR_ACTION_POINTS_UPDATED, "ActionFraction.UpdateActionPoints" )
		WindowUnregisterEventHandler( windowName, SystemData.Events.PLAYER_MAX_ACTION_POINTS_UPDATED, "ActionFraction.UpdateActionPoints" )
		WindowUnRegisterEventHandler( windowName, SystemData.Events.PLAYER_CUR_HIT_POINTS_UPDATED, "ActionFractionWindow.UpdateCurrentHitPoints")
		WindowUnRegisterEventHandler( windowName, SystemData.Events.PLAYER_AGRO_MODE_UPDATED, "ActionFractionWindow.OnAgroModeUpdated")
		WindowUnRegisterEventHandler( windowName, SystemData.Events.PLAYER_HEALTH_FADE_UPDATED, "ActionFractionWindow.UpdateBasedOnUserSettings")
		
		UnregisterEventHandler( SystemData.Events.LOADING_END, "ActionFractionWindow.UpdateVisibility")

		-- Unregister LibSlash commands
		if (LibSlash) then
			LibSlash.UnregisterSlashCmd( "af" )
			LibSlash.UnregisterSlashCmd( "ActionFraction" )
		end

		return hookShutdownPlayInterface( ... )
end

------------------------------------------------------------------
---- Right Click Options Window Functions
------------------------------------------------------------------

function ActionFractionWindow.RightClick()

		-- Define local variables
	  local movable = WindowGetMovable( windowName )
	  local bColorCodeCurrentAP = ActionFraction.Settings.bColorCodeCurrentAP
    local bAutoHide = ActionFraction.Settings.bAutoHide

		-- Create the main context window and display it on a right mouse click
		EA_Window_ContextMenu.CreateContextMenu( windowName, EA_Window_ContextMenu.CONTEXT_MENU_1)
		EA_Window_ContextMenu.AddMenuItem( GetString( StringTables.Default.LABEL_TO_LOCK ), ActionFractionWindow.OnLock, not movable, true, EA_Window_ContextMenu.CONTEXT_MENU_1 )
	  EA_Window_ContextMenu.AddMenuItem( GetString( StringTables.Default.LABEL_TO_UNLOCK ), ActionFractionWindow.OnUnlock, movable, true, EA_Window_ContextMenu.CONTEXT_MENU_1 )   
		EA_Window_ContextMenu.AddMenuDivider (EA_Window_ContextMenu.CONTEXT_MENU_1)
	  EA_Window_ContextMenu.AddCascadingMenuItem( LOC_TEXT.PRESET_LOCATIONS, ActionFractionWindow.SetPresetLocation, false, EA_Window_ContextMenu.CONTEXT_MENU_1 )   
		EA_Window_ContextMenu.AddCascadingMenuItem( LOC_TEXT.SET_FONT_SIZE, ActionFractionWindow.SetFontSelectionMenu, false, EA_Window_ContextMenu.CONTEXT_MENU_1)
		ButtonSetPressedFlag("ActionFractionWindowContextColorCodeCurrentAPCheckBox", bColorCodeCurrentAP)
    EA_Window_ContextMenu.AddUserDefinedMenuItem("ActionFractionWindowContextColorCodeCurrentAP")
		ButtonSetPressedFlag("ActionFractionWindowContextAutoHideCheckBox", bAutoHide)
    EA_Window_ContextMenu.AddUserDefinedMenuItem("ActionFractionWindowContextAutoHide")
		EA_Window_ContextMenu.AddMenuDivider (EA_Window_ContextMenu.CONTEXT_MENU_1)
		EA_Window_ContextMenu.AddMenuItem( GetString( StringTables.Default.LABEL_CHAT_OPTIONS_RESET ), ActionFractionWindow.ResetWindow, false, EA_Window_ContextMenu.CONTEXT_MENU_1)
		EA_Window_ContextMenu.Finalize(EA_Window_ContextMenu.CONTEXT_MENU_1)
end

function ActionFractionWindow.OnLock()
	  WindowSetMovable( windowName, false )
end

function ActionFractionWindow.OnUnlock()
	  WindowSetMovable( windowName, true )
end

function ActionFractionWindow.SetPresetLocation()
		EA_Window_ContextMenu.Hide(EA_Window_ContextMenu.CONTEXT_MENU_3)
		
		EA_Window_ContextMenu.CreateContextMenu("SetPresetLocationMenu", EA_Window_ContextMenu.CONTEXT_MENU_2)
		EA_Window_ContextMenu.AddMenuItem(LOC_TEXT.DEFAULT_APBAR, ActionFractionWindow.SetLocationActionPointBar, false, true, EA_Window_ContextMenu.CONTEXT_MENU_2)
		EA_Window_ContextMenu.AddMenuItem(LOC_TEXT.CENTER_SCREEN, ActionFractionWindow.SetLocationCenterScreen, false, true, EA_Window_ContextMenu.CONTEXT_MENU_2)
		EA_Window_ContextMenu.Finalize(EA_Window_ContextMenu.CONTEXT_MENU_2)
end

function ActionFractionWindow.SetLocationCenterScreen()
		WindowClearAnchors( windowName )
		WindowAddAnchor(windowName, "center",  "Root", "center", 0, 0)
end

function ActionFractionWindow.SetLocationActionPointBar()
		-- Check on the status of the default PlayerWindow
		local showing = WindowGetShowing( "PlayerWindow" ) --PlayerWindowPortrait
		
		-- Make sure the default PlayerWindow exists or is available before moving the ActionFraction window to it
		if (showing == false) or (showing == nil) then
			DialogManager.MakeOneButtonDialog( LOC_TEXT.PLAYERWINDOW_UNAVAILABLE, GetPregameString(StringTables.Pregame.LABEL_CONTINUE) )
		else
			WindowClearAnchors( windowName )
			WindowAddAnchor( windowName, "center", "PlayerWindowStatusContainerAPPercentBar", "center", 2, 6)
		end
end

function ActionFractionWindow.SetFontSelectionMenu()
		EA_Window_ContextMenu.Hide(EA_Window_ContextMenu.CONTEXT_MENU_2)
	
		EA_Window_ContextMenu.CreateContextMenu("SetFontSelectionMenu", EA_Window_ContextMenu.CONTEXT_MENU_3)
		EA_Window_ContextMenu.AddMenuItem(LOC_TEXT.FONT_SIZE_SMALL, ActionFractionWindow.SetFontSmall, false, true, EA_Window_ContextMenu.CONTEXT_MENU_3)
		EA_Window_ContextMenu.AddMenuItem(LOC_TEXT.FONT_SIZE_MEDIUM, ActionFractionWindow.SetFontMedium, false, true, EA_Window_ContextMenu.CONTEXT_MENU_3)
		EA_Window_ContextMenu.AddMenuItem(LOC_TEXT.FONT_SIZE_LARGE, ActionFractionWindow.SetFontLarge, false, true, EA_Window_ContextMenu.CONTEXT_MENU_3)
		EA_Window_ContextMenu.Finalize(EA_Window_ContextMenu.CONTEXT_MENU_3)
end

function ActionFractionWindow.SetFontSmall()
	  LabelSetFont( windowName.."LabelCurrentAP", ActionFractionWindow.FontSizeSmall, WindowUtils.FONT_DEFAULT_TEXT_LINESPACING )
	  LabelSetFont( windowName.."LabelMaximumAP", ActionFractionWindow.FontSizeSmall, WindowUtils.FONT_DEFAULT_TEXT_LINESPACING )
		ActionFraction.Settings.FontSize = ActionFractionWindow.FontSizeSmall
end

function ActionFractionWindow.SetFontMedium()
	  LabelSetFont( windowName.."LabelCurrentAP", ActionFractionWindow.FontSizeMedium, WindowUtils.FONT_DEFAULT_TEXT_LINESPACING )
	  LabelSetFont( windowName.."LabelMaximumAP", ActionFractionWindow.FontSizeMedium, WindowUtils.FONT_DEFAULT_TEXT_LINESPACING )
		ActionFraction.Settings.FontSize = ActionFractionWindow.FontSizeMedium
end

function ActionFractionWindow.SetFontLarge()
	  LabelSetFont( windowName.."LabelCurrentAP", ActionFractionWindow.FontSizeLarge, WindowUtils.FONT_DEFAULT_TEXT_LINESPACING )
	  LabelSetFont( windowName.."LabelMaximumAP", ActionFractionWindow.FontSizeLarge, WindowUtils.FONT_DEFAULT_TEXT_LINESPACING )
		ActionFraction.Settings.FontSize = ActionFractionWindow.FontSizeLarge
end

function ActionFractionWindow.AutoHideOnMouseOver()
		EA_Window_ContextMenu.Hide(EA_Window_ContextMenu.CONTEXT_MENU_3)
end

-- Reset all the variables and values to their defaults
function ActionFractionWindow.ResetWindow()
		-- Reset window dimensions to original
		WindowSetDimensions( windowName, winWidth, winHeight )
		WindowSetDimensions( windowName.."LabelCurrentAP", winWidth/2, winHeight/2 )
		WindowSetDimensions( windowName.."LabelMaximumAP", winWidth/2, winHeight/2 )

		-- Reset window position to center screen
		WindowClearAnchors(windowName)
		WindowSetScale(windowName, InterfaceCore.GetScale())
		WindowAddAnchor(windowName, "center",  "Root", "center", 0, 0)
		
		-- Set the font to default
		ActionFractionWindow.SetFontSmall()
		
		-- Set "Auto-Hide" setting and checkbox
		ActionFraction.Settings.bAutoHide = false
		ButtonSetPressedFlag("ActionFractionWindowContextAutoHideCheckBox", ActionFraction.Settings.bAutoHide)
		
		-- Set "Color Code Current AP" setting and checkbox
		ActionFraction.Settings.bColorCodeCurrentAP = true
		ButtonSetPressedFlag("ActionFractionWindowContextColorCodeCurrentAPCheckBox", ActionFraction.Settings.bColorCodeCurrentAP)
		
		-- Update action points
		ActionFractionWindow.UpdateActionPoints()
end

------------------------------------------------------------------
---- LibSlash Handler Function (LibSlash support optional)
------------------------------------------------------------------

function ActionFraction.SlashHandler(arg)
		local opt, val = arg:match(L"([a-z0-9]+)[ ]?(.*)")

		if not opt then
				-- Output available commands
				TextLogAddEntry( "Chat", 0, towstring(LOC_TEXT.MENU_LIBSLASH_MESSAGE01A) .. towstring(strLibSlashCmds) .. towstring(LOC_TEXT.MENU_LIBSLASH_MESSAGE01B) )

		elseif opt == L"menu" then

				if val == L"" or not val then
						TextLogAddEntry( "Chat", 0, towstring(LOC_TEXT.MENU_LIBSLASH_MESSAGE01A) .. towstring(strLibSlashCmds) .. towstring(LOC_TEXT.MENU_LIBSLASH_MESSAGE01B) )
				else

						if (val:lower() == L"on") or (val:lower() == L"enable") then
								ActionFraction.Settings.bDisableContextMenu = false
								WindowSetHandleInput(windowName, true)
								TextLogAddEntry( "Chat", 0, LOC_TEXT.MENU_LIBSLASH_MESSAGE_ENABLED )
						elseif (val:lower() == L"off") or (val:lower() == L"disable") then
								ActionFraction.Settings.bDisableContextMenu = true
								WindowSetHandleInput(windowName, false)
								TextLogAddEntry( "Chat", 0, LOC_TEXT.MENU_LIBSLASH_MESSAGE_DISABLED )
						else
								TextLogAddEntry( "Chat", 0, towstring(LOC_TEXT.MENU_LIBSLASH_MESSAGE01A) .. towstring(strLibSlashCmds) .. towstring(LOC_TEXT.MENU_LIBSLASH_MESSAGE01B) )
						end
				end

		else
				TextLogAddEntry( "Chat", 0, towstring(LOC_TEXT.MENU_LIBSLASH_MESSAGE01A) .. towstring(strLibSlashCmds) .. towstring(LOC_TEXT.MENU_LIBSLASH_MESSAGE01B) )
		end
end