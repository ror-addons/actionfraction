<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

	<UiMod name="ActionFraction" version="1.7" date="01/30/2012" >
		
		<Author name="Alpha_Male" email="alpha_male@speakeasy.net" />
		<Description text="Displays a floating window which shows the current and maximum Action Points of the player." />

		<VersionSettings gameVersion="1.4.5" windowsVersion="1.0" savedVariablesVersion="1.0" />

		<WARInfo>
			<Categories>
				<Category name="COMBAT" />
				<Category name="OTHER" />
			</Categories>
			<Careers>
				<Career name="ARCHMAGE" />
				<Career name="BLACKGUARD" />
				<Career name="BLACK_ORC" />
				<Career name="BRIGHT_WIZARD" />
				<Career name="CHOPPA" />
				<Career name="CHOSEN" />
				<Career name="DISCIPLE" />
				<Career name="ENGINEER" />
				<Career name="IRON_BREAKER" />
				<Career name="KNIGHT" />
				<Career name="MAGUS" />
				<Career name="MARAUDER" />
				<Career name="RUNE_PRIEST" />
				<Career name="SQUIG_HERDER" />
				<Career name="SHADOW_WARRIOR" />
				<Career name="SHAMAN" />
				<Career name="SORCERER" />
				<Career name="SLAYER" />
				<Career name="SWORDMASTER" />
				<Career name="WARRIOR_PRIEST" />
				<Career name="WHITE_LION" />
				<Career name="WITCH_ELF" />
				<Career name="WITCH_HUNTER" />
				<Career name="ZEALOT" />
			</Careers>
		</WARInfo>

		<Dependencies>
			<Dependency name="EA_PlayerStatusWindow" />
			<Dependency name="EA_UiDebugTools" />
		</Dependencies>

		<Files>
			<File name="language/ActionFractionLocalization.lua" />
			<File name="source/ActionFraction.lua" />
			<File name="source/ActionFraction.xml" />
		</Files>

		<SavedVariables>
			<SavedVariable name="ActionFraction.Settings" global="false"/>
		</SavedVariables>
		
		<OnInitialize>
			<CallFunction name="ActionFraction.Start" />
		</OnInitialize>

	</UiMod>

</ModuleFile>