Title: ActionFraction v1.7

Author: Alpha_Male (alpha_male@speakeasy.net)

Description: Customizable window that displays the total and current action points values for the player.

Features: * Displays Total and Current Action Points
					* Customizable window scale and position
					* Right click options window which includes:
						* Lock/Unlock window location
						* Ability to quickly set display window to preset screen locations
						* Customizable font size
						* Color Code Current Action Point levels
						* Customizable auto hide or fade options (can match Player Window)
						* Reset to defaults
					* All options are saved per character
				  * Optional LibSlash support for expanded menu options
					* Multiple language support

Files: \language\ActionFractionLocalization.lua
 			 \source\ActionFraction.lua
			 \source\ActionFraction.xml
 			 \ActionFraction.mod
 			 \ActionFraction_Install.txt
 			 \ActionFraction_Readme.txt
		
Version History: 1.0 Initial Release
								 1.1 Maintenance Update and Fixes
										 - Version update for latest patch release
										 - Window Alpha and Fade Fixes (entire system rework due to PlayerWindow changes)
							 	 1.2 Maintenance Update
										 - Version update for latest 1.4 patch release
								 1.3 Maintenance Update and New Features
										 - Updated mod version information for 1.4.1
										 - Added color coded current AP functionality and associated right click menu option
								 1.4 Bug Fixes and New Features
										 - Fixed offsets for Preset Location -> Action Bar, should be centered better for all font sizes
										 - Color coded current AP is on by default
										 - Fixed saved setting issue that was causing mod to not load for some people
										 - Added separators for the Right-Click options context menu
								 1.5 Maintenance Update and New Features
										 - Updated mod version information for 1.4.3
										 - Added optional LibSlash support for disabling right-click options menu (makes entire mod window click thru)
										 - Fixes for "Reset to Defaults" saved variables and right-click menu checkbox display
										 - Added additional check for AP changed game events (maximum AP)
								 1.6 Minor Bug Fixes
										 - Hitpoint eventhandler unregister minor fix
								 1.7 Maintenance Update and Minor Bug Fixes
										 - Updated mod version information for 1.4.5
										 - Initialization display string format change
 
Supported Versions: Warhammer Online v1.4.5

Dependencies: None

Future Features: Continued updates for multiple language support.

Known Issues:  None

Additional Credits: None

Special Thanks:	EA/Mythic for a great game and for releasing the API specs
							  The War Wiki (www.thewarwiki.com) for being a great source of knowledge for WAR mod development
								www.curse.com and www.curseforge.com for hosting WAR mod files and projects
							  Trouble INC guild of Badlands for their support

License/Disclaimers: This addon and it's relevant parts and files are released under the 
										 GNU General Public License version 3 (GPLv3).


Additional Notes:  The above information about the addon can also be found in the comment header in the .lua files 
(\source\ActionFraction.lua).

For additional help and support with ActionFraction please visit http://war.curse.com/downloads/war-addons/details/actionfraction.aspx