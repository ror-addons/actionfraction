--	Title: ActionFraction v1.7
--
--	Author: Alpha_Male (alpha_male@speakeasy.net)
--
--	Description: Customizable window that displays the total and current action points values for the player.
--
--	Features: * Displays Total and Current Action Points
--						* Customizable window scale and position
--						* Right click options window which includes:
--							* Lock/Unlock window location
--							* Ability to quickly set display window to preset screen locations
--							* Customizable font size
--							* Color Code Current Action Point levels
--							* Customizable auto hide or fade options (can match Player Window)
--							* Reset to defaults
--						* All options are saved per character
--						* Optional LibSlash support for expanded menu options
--						* Multiple language support
--
--	Files: \language\ActionFractionLocalization.lua
--	 			 \source\ActionFraction.lua
--				 \source\ActionFraction.xml
--	 			 \ActionFraction.mod
--	 			 \ActionFraction_Install.txt
--	 			 \ActionFraction_Readme.txt
--			
--	Version History: 1.0 Initial Release
--									 1.1 Maintenance Update and Fixes
--											 - Version update for latest patch release
--											 - Window Alpha and Fade Fixes (entire system rework due to PlayerWindow changes)
--								 	 1.2 Maintenance Update
--											 - Version update for latest 1.4 patch release
--									 1.3 Maintenance Update and New Features
--											 - Updated mod version information for 1.4.1
--											 - Added color coded current AP functionality and associated right click menu option
--									 1.4 Bug Fixes and New Features
--											 - Fixed offsets for Preset Location -> Action Bar, should be centered better for all font sizes
--											 - Color coded current AP is on by default
--											 - Fixed saved setting issue that was causing mod to not load for some people
--											 - Added separators for the Right-Click options context menu
--									 1.5 Maintenance Update and New Features
--											 - Updated mod version information for 1.4.3
--											 - Added optional LibSlash support for disabling right-click options menu (makes entire mod window click thru)
--											 - Fixes for "Reset to Defaults" saved variables and right-click menu checkbox display
--											 - Added additional check for AP changed game events (maximum AP)
--									 1.6 Minor Bug Fixes
--											 - Hitpoint eventhandler unregister minor fix
--									 1.7 Maintenance Update and Minor Bug Fixes
--											 - Updated mod version information for 1.4.5
--											 - Initialization display string format change
--
--  Supported Versions: Warhammer Online v1.4.5
--
--	Dependencies: None
--
--	Future Features: Continued updates for multiple language support.
--
--	Known Issues:  None
--
--	Additional Credits: None
--
--	Special Thanks:	EA/Mythic for a great game and for releasing the API specs
--								  The War Wiki (www.thewarwiki.com) for being a great source of knowledge for WAR mod development
--									www.curse.com and www.curseforge.com for hosting WAR mod files and projects
--								  Trouble INC guild of Badlands for their support
--
--	License/Disclaimers: This addon and it's relevant parts and files are released under the 
--											 GNU General Public License version 3 (GPLv3).
--

------------------------------------------------------------------
----  Global Variables
------------------------------------------------------------------

if not ActionFraction then
	ActionFraction = {}
end

ActionFraction.wstrings = {}

------------------------------------------------------------------
----  ENGLISH
------------------------------------------------------------------

ActionFraction.wstrings[SystemData.Settings.Language.ENGLISH] = {
	SET_FONT_SIZE 		= L"Set Font Size",
	FONT_SIZE_SMALL 	= L"Small",
	FONT_SIZE_MEDIUM 	= L"Medium",
	FONT_SIZE_LARGE 	= L"Large",
	CURRENTAP_COLOR_CODING 	= L"Color Code Current AP",
	PRESET_LOCATIONS 	= L"Preset Locations",
	DEFAULT_APBAR 		= L"Action Point Bar",
	CENTER_SCREEN			= L"Screen Center",
	PLAYERWINDOW_UNAVAILABLE = L"The player window is unavailable, it is either not showing or has been disabled.",
	ERROR_LIBSLASH_MESSAGE01 = L"(ActionFraction) WARNING: LibSlash Command \"af\" already registered.",
	ERROR_LIBSLASH_MESSAGE02 = L"(ActionFraction) WARNING: LibSlash Command \"ActionFraction\" already registered.",
	MENU_LIBSLASH_MESSAGE01A = L"(ActionFraction) Use ",
	MENU_LIBSLASH_MESSAGE01B = L" menu enable|disable to allow context menu options when right clicking ActionFraction display (allow click through).",
	MENU_LIBSLASH_MESSAGE_ENABLED = L"(ActionFraction) Context menu when right clicking ActionFraction display: ENABLED.",
	MENU_LIBSLASH_MESSAGE_DISABLED = L"(ActionFraction) Context menu when right clicking ActionFraction display: DISABLED (click through window).",
}

------------------------------------------------------------------
----  FRENCH
------------------------------------------------------------------

ActionFraction.wstrings[SystemData.Settings.Language.FRENCH] = {
	SET_FONT_SIZE 		= L"Placez la taille de police",
	FONT_SIZE_SMALL 	= L"Petit",
	FONT_SIZE_MEDIUM 	= L"Milieu",
	FONT_SIZE_LARGE 	= L"Grand",
	CURRENTAP_COLOR_CODING 	= L"Code couleurs AP courant",
	PRESET_LOCATIONS 	= L"Endroits de pr�r�glage",
	DEFAULT_APBAR 		= L"Barre d'action",
	CENTER_SCREEN			= L"Centre d'�cran",
	PLAYERWINDOW_UNAVAILABLE = L"La fen�tre de joueur est indisponible, elle ne montre pas ou a �t� d�sactiv�e.",
	ERROR_LIBSLASH_MESSAGE01 = L"(ActionFraction) AVERTISSEMENT: LibSlash Commande \"af\" d�j� enregistr�.",
	ERROR_LIBSLASH_MESSAGE02 = L"(ActionFraction) AVERTISSEMENT: LibSlash Commande \"ActionFraction\" d�j� enregistr�.",
	MENU_LIBSLASH_MESSAGE01A = L"(ActionFraction) Utilisez ",
	MENU_LIBSLASH_MESSAGE01B = L" menu enable|disable pour permettre options du menu contextuel clic droit affichage ActionFraction (permettre cliquer sur).",
	MENU_LIBSLASH_MESSAGE_ENABLED = L"(ActionFraction) du menu contextuel lorsque l'affichage ActionFraction un clic droit: PERMIS.",
	MENU_LIBSLASH_MESSAGE_DISABLED = L"(ActionFraction) du menu contextuel lorsque l'affichage ActionFraction un clic droit: HANDICAPES (cliquer sur la fen�tre).",
}

------------------------------------------------------------------
----  GERMAN
------------------------------------------------------------------

ActionFraction.wstrings[SystemData.Settings.Language.GERMAN] = {
	SET_FONT_SIZE 		= L"Stellen Sie Guss-Gr��e ein",
	FONT_SIZE_SMALL 	= L"Klein",
	FONT_SIZE_MEDIUM 	= L"Mittel",
	FONT_SIZE_LARGE 	= L"Gro�",
	CURRENTAP_COLOR_CODING 	= L"Farben-Code gegenw�rtiges AP",
	PRESET_LOCATIONS 	= L"Voreinstellungs-Positionen",
	DEFAULT_APBAR 		= L"Druckpunkt-Stab",
	CENTER_SCREEN			= L"Schirm-Mitte",
	PLAYERWINDOW_UNAVAILABLE = L"Das Spielerfenster ist nicht erreichbar, stellt es entweder nicht dar oder ist gesperrt worden.",
	ERROR_LIBSLASH_MESSAGE01 = L"(ActionFraction) ACHTUNG: LibSlash Befehl \"af\" bereits registriert.",
	ERROR_LIBSLASH_MESSAGE02 = L"(ActionFraction) ACHTUNG: LibSlash Befehl \"ActionFraction\" bereits registriert.",
	MENU_LIBSLASH_MESSAGE01A = L"(ActionFraction) Verwenden ",
	MENU_LIBSLASH_MESSAGE01B = L" men� enable|disable, damit kontextmen� der rechten maustaste auf optionen, wenn ActionFraction Display (lassen sie durch).",
	MENU_LIBSLASH_MESSAGE_ENABLED = L"(ActionFraction) Kontextmen� der rechten maustaste, wenn ActionFraction anzeige: AKTIVIERT.",
	MENU_LIBSLASH_MESSAGE_DISABLED = L"(ActionFraction) Kontextmen� der rechten maustaste, wenn ActionFraction anzeige: DEAKTIVIERT (click-through-fenster).",
}

------------------------------------------------------------------
----  ITALIAN
------------------------------------------------------------------

ActionFraction.wstrings[SystemData.Settings.Language.ITALIAN] = {
	SET_FONT_SIZE 		= L"Fissi la dimensione",
	FONT_SIZE_SMALL 	= L"Piccolo",
	FONT_SIZE_MEDIUM 	= L"Mezzo",
	FONT_SIZE_LARGE 	= L"Grande",
	CURRENTAP_COLOR_CODING 	= L"Codice di colore AP corrente",
	PRESET_LOCATIONS 	= L"Posizioni di preregolamento",
	DEFAULT_APBAR 		= L"Barra del punto di azione",
	CENTER_SCREEN			= L"Centro dello schermo",
	PLAYERWINDOW_UNAVAILABLE = L"La finestra del giocatore � non disponibile, non sta mostrando o � stata disabile.",
	ERROR_LIBSLASH_MESSAGE01 = L"(ActionFraction) ATTENZIONE: il Comando LibSlash \"af\" gi� registrato.",
	ERROR_LIBSLASH_MESSAGE02 = L"(ActionFraction) ATTENZIONE: il Comando LibSlash \"ActionFraction\" gi� registrato.",
	MENU_LIBSLASH_MESSAGE01A = L"(ActionFraction) Utilizzare ",
	MENU_LIBSLASH_MESSAGE01B = L" menu consentono|disabilitare per permettere le opzioni di menu contestuale quando fai clic destro display ActionFraction (consentire click through).",
	MENU_LIBSLASH_MESSAGE_ENABLED = L"(ActionFraction) men� Contestuale display ActionFraction clic destro: ABILITATO.",
	MENU_LIBSLASH_MESSAGE_DISABLED = L"(ActionFraction) men� Contestuale display ActionFraction clic destro: DISABILI (clicca attraverso la finestra).",
}

------------------------------------------------------------------
----  SPANISH
------------------------------------------------------------------

ActionFraction.wstrings[SystemData.Settings.Language.SPANISH] = {
	SET_FONT_SIZE 		= L"Fije el tama�o de fuente",
	FONT_SIZE_SMALL 	= L"Peque�o",
	FONT_SIZE_MEDIUM 	= L"Medio",
	FONT_SIZE_LARGE 	= L"Grande",
	CURRENTAP_COLOR_CODING 	= L"C�digo de color AP actual",
	PRESET_LOCATIONS 	= L"Localizaciones de la precolocaci�n",
	DEFAULT_APBAR 		= L"Barra del punto de acci�n",
	CENTER_SCREEN			= L"Centro de la pantalla",
	PLAYERWINDOW_UNAVAILABLE = L"La ventana del jugador es inasequible, no est� demostrando ni se ha inhabilitado.", -- Localization Required
	ERROR_LIBSLASH_MESSAGE01 = L"(ActionFraction) ADVERTENCIA: LibSlash Comando \"af\" ya est� registrado.",
	ERROR_LIBSLASH_MESSAGE02 = L"(ActionFraction) ADVERTENCIA: LibSlash Comando \"ActionFraction\" ya est� registrado.",
	MENU_LIBSLASH_MESSAGE01A = L"ActionFraction) Utilice ",
	MENU_LIBSLASH_MESSAGE01B = L" men� enable|disable para permitir que las opciones del men� contextual al hacer clic derecho ActionFraction pantalla (permite hacer clic a trav�s).",
	MENU_LIBSLASH_MESSAGE_ENABLED = L"(ActionFraction) men� Contextual al hacer clic derecho pantalla ActionFraction: HABILITADO.",
	MENU_LIBSLASH_MESSAGE_DISABLED = L"(ActionFraction) men� Contextual al hacer clic derecho pantalla ActionFraction: Prohibido (a trav�s de la ventana).",
}

------------------------------------------------------------------
----  KOREAN
------------------------------------------------------------------

ActionFraction.wstrings[SystemData.Settings.Language.KOREAN] = {
	SET_FONT_SIZE 		= L"Set Font Size", -- Localization Required
	FONT_SIZE_SMALL 	= L"Small", -- Localization Required
	FONT_SIZE_MEDIUM 	= L"Medium", -- Localization Required
	FONT_SIZE_LARGE 	= L"Large", -- Localization Required
	CURRENTAP_COLOR_CODING 	= L"Color Code Current AP", -- Localization Required
	PRESET_LOCATIONS 	= L"Preset Locations", -- Localization Required
	DEFAULT_APBAR 		= L"Action Point Bar", -- Localization Required
	CENTER_SCREEN			= L"Screen Center", -- Localization Required
	PLAYERWINDOW_UNAVAILABLE = L"The player window is unavailable, it is either not showing or has been disabled.", -- Localization Required
	ERROR_LIBSLASH_MESSAGE01 = L"(ActionFraction) WARNING: LibSlash Command \"af\" already registered.", -- Localization Required
	ERROR_LIBSLASH_MESSAGE02 = L"(ActionFraction) WARNING: LibSlash Command \"ActionFraction\" already registered.", -- Localization Required
	MENU_LIBSLASH_MESSAGE01A = L"(ActionFraction) Use ", -- Localization Required
	MENU_LIBSLASH_MESSAGE01B = L" menu enable|disable to allow context menu options when right clicking ActionFraction display (allow click through).", -- Localization Required
	MENU_LIBSLASH_MESSAGE_ENABLED = L"(ActionFraction) Context menu when right clicking ActionFraction display: ENABLED.", -- Localization Required
	MENU_LIBSLASH_MESSAGE_DISABLED = L"(ActionFraction) Context menu when right clicking ActionFraction display: DISABLED (click through window).", -- Localization Required
}

------------------------------------------------------------------
----  S_CHINESE
------------------------------------------------------------------

ActionFraction.wstrings[SystemData.Settings.Language.S_CHINESE] = {
	SET_FONT_SIZE 		= L"Set Font Size", -- Localization Required
	FONT_SIZE_SMALL 	= L"Small", -- Localization Required
	FONT_SIZE_MEDIUM 	= L"Medium", -- Localization Required
	FONT_SIZE_LARGE 	= L"Large", -- Localization Required
	CURRENTAP_COLOR_CODING 	= L"Color Code Current AP", -- Localization Required
	PRESET_LOCATIONS 	= L"Preset Locations", -- Localization Required
	DEFAULT_APBAR 		= L"Action Point Bar", -- Localization Required
	CENTER_SCREEN			= L"Screen Center", -- Localization Required
	PLAYERWINDOW_UNAVAILABLE = L"The player window is unavailable, it is either not showing or has been disabled.", -- Localization Required
	ERROR_LIBSLASH_MESSAGE01 = L"(ActionFraction) WARNING: LibSlash Command \"af\" already registered.", -- Localization Required
	ERROR_LIBSLASH_MESSAGE02 = L"(ActionFraction) WARNING: LibSlash Command \"ActionFraction\" already registered.", -- Localization Required
	MENU_LIBSLASH_MESSAGE01A = L"(ActionFraction) Use ", -- Localization Required
	MENU_LIBSLASH_MESSAGE01B = L" menu enable|disable to allow context menu options when right clicking ActionFraction display (allow click through).", -- Localization Required
	MENU_LIBSLASH_MESSAGE_ENABLED = L"(ActionFraction) Context menu when right clicking ActionFraction display: ENABLED.", -- Localization Required
	MENU_LIBSLASH_MESSAGE_DISABLED = L"(ActionFraction) Context menu when right clicking ActionFraction display: DISABLED (click through window).", -- Localization Required
}

------------------------------------------------------------------
----  T_CHINESE
------------------------------------------------------------------

ActionFraction.wstrings[SystemData.Settings.Language.T_CHINESE] = {
	SET_FONT_SIZE 		= L"Set Font Size", -- Localization Required
	FONT_SIZE_SMALL	 	= L"Small", -- Localization Required
	FONT_SIZE_MEDIUM 	= L"Medium", -- Localization Required
	FONT_SIZE_LARGE 	= L"Large", -- Localization Required
	CURRENTAP_COLOR_CODING 	= L"Color Code Current AP", -- Localization Required
	PRESET_LOCATIONS 	= L"Preset Locations", -- Localization Required
	DEFAULT_APBAR 		= L"Action Point Bar", -- Localization Required
	CENTER_SCREEN			= L"Screen Center", -- Localization Required
	PLAYERWINDOW_UNAVAILABLE = L"The player window is unavailable, it is either not showing or has been disabled.", -- Localization Required
	ERROR_LIBSLASH_MESSAGE01 = L"(ActionFraction) WARNING: LibSlash Command \"af\" already registered.", -- Localization Required
	ERROR_LIBSLASH_MESSAGE02 = L"(ActionFraction) WARNING: LibSlash Command \"ActionFraction\" already registered.", -- Localization Required
	MENU_LIBSLASH_MESSAGE01A = L"(ActionFraction) Use ", -- Localization Required
	MENU_LIBSLASH_MESSAGE01B = L" menu enable|disable to allow context menu options when right clicking ActionFraction display (allow click through).", -- Localization Required
	MENU_LIBSLASH_MESSAGE_ENABLED = L"(ActionFraction) Context menu when right clicking ActionFraction display: ENABLED.", -- Localization Required
	MENU_LIBSLASH_MESSAGE_DISABLED = L"(ActionFraction) Context menu when right clicking ActionFraction display: DISABLED (click through window).", -- Localization Required
}

------------------------------------------------------------------
----  JAPANESE
------------------------------------------------------------------

ActionFraction.wstrings[SystemData.Settings.Language.JAPANESE] = {
	SET_FONT_SIZE 		= L"Set Font Size", -- Localization Required
	FONT_SIZE_SMALL 	= L"Small", -- Localization Required
	FONT_SIZE_MEDIUM 	= L"Medium", -- Localization Required
	FONT_SIZE_LARGE 	= L"Large", -- Localization Required
	CURRENTAP_COLOR_CODING 	= L"Color Code Current AP", -- Localization Required
	PRESET_LOCATIONS	= L"Preset Locations", -- Localization Required
	DEFAULT_APBAR 		= L"Action Point Bar", -- Localization Required
	CENTER_SCREEN			= L"Screen Center", -- Localization Required
	PLAYERWINDOW_UNAVAILABLE = L"The player window is unavailable, it is either not showing or has been disabled.", -- Localization Required
	ERROR_LIBSLASH_MESSAGE01 = L"(ActionFraction) WARNING: LibSlash Command \"af\" already registered.", -- Localization Required
	ERROR_LIBSLASH_MESSAGE02 = L"(ActionFraction) WARNING: LibSlash Command \"ActionFraction\" already registered.", -- Localization Required
	MENU_LIBSLASH_MESSAGE01A = L"(ActionFraction) Use ", -- Localization Required
	MENU_LIBSLASH_MESSAGE01B = L" menu enable|disable to allow context menu options when right clicking ActionFraction display (allow click through).", -- Localization Required
	MENU_LIBSLASH_MESSAGE_ENABLED = L"(ActionFraction) Context menu when right clicking ActionFraction display: ENABLED.", -- Localization Required
	MENU_LIBSLASH_MESSAGE_DISABLED = L"(ActionFraction) Context menu when right clicking ActionFraction display: DISABLED (click through window).", -- Localization Required
}

------------------------------------------------------------------
----  RUSSIAN
------------------------------------------------------------------

ActionFraction.wstrings[SystemData.Settings.Language.RUSSIAN] = {
	SET_FONT_SIZE 		= L"Set Font Size", -- Localization Required
	FONT_SIZE_SMALL 	= L"Small", -- Localization Required
	FONT_SIZE_MEDIUM 	= L"Medium", -- Localization Required
	FONT_SIZE_LARGE 	= L"Large", -- Localization Required
	CURRENTAP_COLOR_CODING 	= L"Color Code Current AP", -- Localization Required
	PRESET_LOCATIONS 	= L"Preset Locations", -- Localization Required
	DEFAULT_APBAR 		= L"Action Point Bar", -- Localization Required
	CENTER_SCREEN			= L"Screen Center", -- Localization Required
	PLAYERWINDOW_UNAVAILABLE = L"The player window is unavailable, it is either not showing or has been disabled.", -- Localization Required
	ERROR_LIBSLASH_MESSAGE01 = L"(ActionFraction) WARNING: LibSlash Command \"af\" already registered.", -- Localization Required
	ERROR_LIBSLASH_MESSAGE02 = L"(ActionFraction) WARNING: LibSlash Command \"ActionFraction\" already registered.", -- Localization Required
	MENU_LIBSLASH_MESSAGE01A = L"(ActionFraction) Use ",
	MENU_LIBSLASH_MESSAGE01B = L" menu enable|disable to allow context menu options when right clicking ActionFraction display (allow click through).", -- Localization Required
	MENU_LIBSLASH_MESSAGE_ENABLED = L"(ActionFraction) Context menu when right clicking ActionFraction display: ENABLED.", -- Localization Required
	MENU_LIBSLASH_MESSAGE_DISABLED = L"(ActionFraction) Context menu when right clicking ActionFraction display: DISABLED (click through window).", -- Localization Required
}